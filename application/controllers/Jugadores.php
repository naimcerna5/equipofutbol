<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jugadores extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("Jugador"); // Cargar el modelo Jugador
        $this->load->model("Posicion"); // Cargar el modelo Posicion para obtener las posiciones
        $this->load->model("Equipo"); // Cargar el modelo Equipo para obtener los equipos

        // Deshabilitar errores y advertencias de PHP
        error_reporting(0);
    }

    public function index() {
        $data["listadoJugadores"] = $this->Jugador->consultarTodos();
        $this->load->view("header");
        $this->load->view("jugadores/index", $data);
        $this->load->view("footer");
    }

    // Eliminación de jugadores recibiendo el ID por GET
    public function borrar($id_jug) {
        $this->Jugador->eliminar($id_jug); // Llamar al método eliminar del modelo Jugador
        $this->session->set_flashdata("confirmacion", "Jugador eliminado correctamente");
        redirect("jugadores/index");
    }

    // Renderización del formulario para nuevo jugador
    public function nuevo() {
        $data["posiciones"] = $this->Posicion->consultarTodos();
        $data["equipos"] = $this->Equipo->consultarTodos();

        $this->load->view("header");
        $this->load->view("jugadores/nuevo", $data);
        $this->load->view("footer");
    }

    // Captura de datos e inserción en Jugador
    public function guardarJugador() {
        $datosNuevoJugador = array(
            "apellido_jug" => $this->input->post("apellido_jug"),
            "nombre_jug" => $this->input->post("nombre_jug"),
            "estatura_jug" => $this->input->post("estatura_jug"),
            "salario_jug" => $this->input->post("salario_jug"),
            "estado_jug" => $this->input->post("estado_jug"),
            "fk_id_pos" => $this->input->post("fk_id_pos"),
            "fk_id_equi" => $this->input->post("fk_id_equi"),
        );

        // Insertar el nuevo jugador en la base de datos
        $this->Jugador->insertar($datosNuevoJugador);
        $this->session->set_flashdata("confirmacion", "Jugador guardado exitosamente");
        redirect('jugadores/index');
    }

    // Renderización del formulario para editar jugador
    public function editar($id_jug) {
        $data["jugadorEditar"] = $this->Jugador->obtenerPorId($id_jug);
        $data["posiciones"] = $this->Posicion->consultarTodos();
        $data["equipos"] = $this->Equipo->consultarTodos();

        $this->load->view("header");
        $this->load->view("jugadores/editar", $data);
        $this->load->view("footer");
    }

    // Actualización del jugador en la base de datos
    public function actualizarJugador() {
        $id_jug = $this->input->post("id_jug");
        $datosJugador = array(
            "apellido_jug" => $this->input->post("apellido_jug"),
            "nombre_jug" => $this->input->post("nombre_jug"),
            "estatura_jug" => $this->input->post("estatura_jug"),
            "salario_jug" => $this->input->post("salario_jug"),
            "estado_jug" => $this->input->post("estado_jug"),
            "fk_id_pos" => $this->input->post("fk_id_pos"),
            "fk_id_equi" => $this->input->post("fk_id_equi"),
        );

        $this->Jugador->actualizar($id_jug, $datosJugador);
        $this->session->set_flashdata("confirmacion", "Jugador actualizado exitosamente");
        redirect('jugadores/index');
    }
}
?>
