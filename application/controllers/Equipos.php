<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Equipos extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("Equipo"); // Cargar el modelo Equipo

        // Deshabilitar errores y advertencias de PHP
        error_reporting(0);
    }

    public function index() {
        $data["listadoEquipos"] = $this->Equipo->consultarTodos();
        $this->load->view("header");
        $this->load->view("equipos/index", $data);
        $this->load->view("footer");
    }

    // Eliminación de equipos recibiendo el ID por GET
    public function borrar($id_equi) {
        $this->Equipo->eliminar($id_equi); // Llamar al método eliminar del modelo Equipo
        $this->session->set_flashdata("confirmacion", "Equipo eliminado correctamente");

        redirect("equipos/index");
    }

    // Renderización del formulario para nuevo equipo
    public function nuevo() {
        $this->load->view("header");
        $this->load->view("equipos/nuevo");
        $this->load->view("footer");
    }

    // Captura de datos e inserción en Equipo
    public function guardarEquipo() {
        $datosNuevoEquipo = array(
            "nombre_equi" => $this->input->post("nombre_equi"),
            "siglas_equi" => $this->input->post("siglas_equi"),
            "fundacion_equi" => $this->input->post("fundacion_equi"),
            "region_equi" => $this->input->post("region_equi"),
            "numero_titulos_equi" => $this->input->post("numero_titulos_equi"),
        );

        // Insertar el nuevo equipo en la base de datos
        $this->Equipo->insertar($datosNuevoEquipo);
        $this->session->set_flashdata("confirmacion", "Equipo guardado exitosamente");

        redirect('equipos/index');
    }

    // Renderización del formulario para editar equipo
    public function editar($id_equi) {
        $data["equipoEditar"] = $this->Equipo->obtenerPorId($id_equi);
        $this->load->view("header");
        $this->load->view("equipos/editar", $data);
        $this->load->view("footer");
    }

    // Actualización del equipo en la base de datos
    public function actualizarEquipo() {
        $id_equi = $this->input->post("id_equi");
        $datosEquipo = array(
            "nombre_equi" => $this->input->post("nombre_equi"),
            "siglas_equi" => $this->input->post("siglas_equi"),
            "fundacion_equi" => $this->input->post("fundacion_equi"),
            "region_equi" => $this->input->post("region_equi"),
            "numero_titulos_equi" => $this->input->post("numero_titulos_equi"),
        );
        $this->Equipo->actualizar($id_equi, $datosEquipo);
        $this->session->set_flashdata("confirmacion", "Equipo actualizado exitosamente");

        redirect('equipos/index');
    }

} // Fin de la clase
?>
