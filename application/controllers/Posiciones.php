<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Posiciones extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("Posicion"); // Cargar el modelo Posicion

        // Deshabilitar errores y advertencias de PHP
        error_reporting(0);
    }

    public function index() {
        $data["listadoPosiciones"] = $this->Posicion->consultarTodos();
        $this->load->view("header");
        $this->load->view("posiciones/index", $data);
        $this->load->view("footer");
    }

    // Eliminación de posiciones recibiendo el ID por GET
    public function borrar($id_pos) {
        $this->Posicion->eliminar($id_pos); // Llamar al método eliminar del modelo Posicion
        $this->session->set_flashdata("confirmacion", "Posición eliminada correctamente");

        redirect("posiciones/index");
    }

    // Renderización del formulario para nueva posición
    public function nuevo() {
        $this->load->view("header");
        $this->load->view("posiciones/nuevo");
        $this->load->view("footer");
    }

    // Captura de datos e inserción en Posicion
    public function guardarPosicion() {

        $datosNuevaPosicion = array(
            "nombre_pos" => $this->input->post("nombre_pos"),
            "descripcion_pos" => $this->input->post("descripcion_pos"),
        );

        // Insertar la nueva posición en la base de datos
        $this->Posicion->insertar($datosNuevaPosicion);
        $this->session->set_flashdata("confirmacion", "Posición guardada exitosamente");

        // Ejemplo de envío de email (debes definir la función enviarEmail)
        enviarEmail("brandon.gordon8932@utc.edu.ec", "CREACION",
            "<h1>SE CREO LA POSICION </h1>" . $datosNuevaPosicion['nombre_pos']);

        redirect('posiciones/index');
    }

    // Renderización del formulario para editar posición
    public function editar($id_pos) {
        $data["posicionEditar"] = $this->Posicion->obtenerPorId($id_pos);
        $this->load->view("header");
        $this->load->view("posiciones/editar", $data);
        $this->load->view("footer");
    }

    // Actualización de la posición en la base de datos
    public function actualizarPosicion() {
        $id_pos = $this->input->post("id_pos");
        $datosPosicion = array(
            "nombre_pos" => $this->input->post("nombre_pos"),
            "descripcion_pos" => $this->input->post("descripcion_pos")
        );
        $this->Posicion->actualizar($id_pos, $datosPosicion);
        $this->session->set_flashdata("confirmacion", "Posición actualizada exitosamente");

        redirect('posiciones/index');
    }

} // Fin de la clase
?>
