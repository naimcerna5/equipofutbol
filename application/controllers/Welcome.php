<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
//renderizando  vista welcome_message con header y footer
	public function index()
	{
		$this->load->view('header');//Cargando Cabecera
		$this->load->view('welcome_message');//Cargando Contenido
		$this->load->view('footer');//Cargando Pie
	}
}
