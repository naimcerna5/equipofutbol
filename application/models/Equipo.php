<?php
class Equipo extends CI_Model
{
  function __construct()
  {
    parent::__construct();
  }

  // Insertar nuevo equipo
  function insertar($datos){
    return $this->db->insert("equipo", $datos);
  }

  // Consultar todos los equipos
  function consultarTodos(){
    $equipos = $this->db->get("equipo");
    if ($equipos->num_rows() > 0){
      return $equipos->result();
    } else {
      return false;
    }
  }

  // Eliminar equipo por ID
  function eliminar($id){
    $this->db->where("id_equi", $id);
    return $this->db->delete("equipo");
  }

  // Consultar un solo equipo por ID
  function obtenerPorId($id){
    $this->db->where("id_equi", $id);
    $equipo = $this->db->get("equipo");
    if ($equipo->num_rows() > 0) {
      return $equipo->row();
    } else {
      return false;
    }
  }

  // Actualizar equipo
  function actualizar($id, $datos){
    $this->db->where("id_equi", $id);
    return $this->db->update("equipo", $datos);
  }
}
?>
