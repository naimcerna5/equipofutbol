<?php
class Jugador extends CI_MODEL {

    function __construct() {
        parent::__construct();
    }

    // Insertar nuevo jugador
    function insertar($datos) {
        return $this->db->insert("jugador", $datos);
    }

    // Consultar todos los jugadores
    function consultarTodos() {
        $this->db->select('jugador.*, posicion.nombre_pos, equipo.nombre_equi');
        $this->db->from('jugador');
        $this->db->join('posicion', 'posicion.id_pos = jugador.fk_id_pos', 'left');
        $this->db->join('equipo', 'equipo.id_equi = jugador.fk_id_equi', 'left');
        $query = $this->db->get();
        
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    // Eliminar jugador por ID
    function eliminar($id) {
        $this->db->where("id_jug", $id);
        return $this->db->delete("jugador");
    }

    // Consultar un solo jugador por ID
    function obtenerPorId($id) {
        $this->db->where("id_jug", $id);
        $query = $this->db->get("jugador");
        
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    // Actualizar jugador
    function actualizar($id, $datos) {
        $this->db->where("id_jug", $id);
        return $this->db->update("jugador", $datos);
    }
}
?>
