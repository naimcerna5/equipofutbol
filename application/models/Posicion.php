<?php
class Posicion extends CI_MODEL
{

  function __construct()
  {
    parent::__construct();
  }

  // Insertar nuevas posiciones
  function insertar($datos){
    $respuesta = $this->db->insert("posicion", $datos);
    return $respuesta;
  }

  // Consultar todas las posiciones
  function consultarTodos(){
    $posiciones = $this->db->get("posicion");
    if ($posiciones->num_rows() > 0){
      return $posiciones->result();
    } else {
      return false;
    }
  }

  // Eliminar posición por ID
  function eliminar($id){
    $this->db->where("id_pos", $id); // Corregido: usar id_pos en lugar de id_hos
    return $this->db->delete("posicion");
  }

  // Consultar una sola posición por ID
  function obtenerPorId($id){
    $this->db->where("id_pos", $id); // Corregido: usar id_pos en lugar de id_hos
    $posicion = $this->db->get("posicion");
    if ($posicion->num_rows() > 0) {
      return $posicion->row();
    } else {
      return false;
    }
  }

  // Actualizar posición
  function actualizar($id, $datos){
    $this->db->where("id_pos", $id); // Corregido: usar id_pos en lugar de id_hos
    return $this->db->update("posicion", $datos);
  }

} // fin de la clase Posicion
?>
