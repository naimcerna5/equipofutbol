<h1>EDITAR EQUIPO</h1>
<form class="" method="post" action="<?php echo site_url('equipos/actualizarEquipo'); ?>" id="frm_nuevo_equipo">
  <input type="hidden" name="id_equi" id="id_equi" value="<?php echo $equipoEditar->id_equi; ?>">

  <label for="nombre_equi"><b>Nombre:</b></label>
  <input type="text" name="nombre_equi" id="nombre_equi" value="<?php echo $equipoEditar->nombre_equi; ?>" placeholder="Ingrese el nombre del equipo..." class="form-control" required>
  <br>

  <label for="siglas_equi"><b>Siglas:</b></label>
  <input type="text" name="siglas_equi" id="siglas_equi" value="<?php echo $equipoEditar->siglas_equi; ?>" placeholder="Ingrese las siglas del equipo..." class="form-control" required>
  <br>

  <label for="fundacion_equi"><b>Año de Fundación:</b></label>
  <input type="number" name="fundacion_equi" id="fundacion_equi" value="<?php echo $equipoEditar->fundacion_equi; ?>" placeholder="Ingrese el año de fundación del equipo..." class="form-control" required>
  <br>

  <label for="region_equi"><b>Región:</b></label>
  <input type="text" name="region_equi" id="region_equi" value="<?php echo $equipoEditar->region_equi; ?>" placeholder="Ingrese la región del equipo..." class="form-control" required>
  <br>

  <label for="numero_titulos_equi"><b>Número de Títulos:</b></label>
  <input type="number" name="numero_titulos_equi" id="numero_titulos_equi" value="<?php echo $equipoEditar->numero_titulos_equi; ?>" placeholder="Ingrese el número de títulos del equipo..." class="form-control" required>
  <br>

  <div class="row">
    <div class="col-md-12 text-center">
      <button type="submit" name="button" class="btn btn-primary">
        <i class="fa fa-pen"></i> &nbsp; ACTUALIZAR
      </button>&nbsp;&nbsp;
      <a href="<?php echo site_url('equipos/index'); ?>" class="btn btn-danger">
        <i class="fa fa-times-circle"></i> &nbsp; Cancelar
      </a>
    </div>
  </div>
</form>

<br><br>
<script>
     $.validator.addMethod("lettersonly", function(value, element) {
    return this.optional(element) || /^[a-zA-Z\s]+$/i.test(value);
  }, "DEBE CONTENER SOLO LETRAS");

$("#frm_nuevo_equipo").validate({
     rules: {
        "nombre_equi": {
          required: true,
          lettersonly: true,
          minlength: 2,
          maxlength: 50
        },
        "siglas_equi": {
          required: true,
          minlength: 2,
          maxlength: 25
        },
        "fundacion_equi": {
          required: true,
          digits: true,
          minlength: 4,
          maxlength: 4
        },
       "region_equi": {
          required: true,
          lettersonly: true,
          minlength: 2,
          maxlength: 25
        },
        "numero_titulos_equi": {
          required: true,
          digits: true
        }
      },
      messages: {
        "nombre_equi": {
          required: "DEBE INGRESAR EL NOMBRE DEL EQUIPO",
          lettersonly: "DEBE CONTENER SOLO LETRAS",
          minlength: "EL NOMBRE DEBE TENER AL MENOS 2 CARACTERES",
          maxlength: "EL NOMBRE NO PUEDE TENER MÁS DE 50 CARACTERES"
        },
        "siglas_equi": {
          required: "DEBE INGRESAR LAS SIGLAS DEL EQUIPO",
          minlength: "LAS SIGLAS DEBEN TENER AL MENOS 2 CARACTERES",
          maxlength: "LAS SIGLAS NO PUEDEN TENER MÁS DE 25 CARACTERES"
        },
        "fundacion_equi": {
          required: "DEBE INGRESAR EL AÑO DE FUNDACIÓN",
          digits: "EL AÑO DEBE SER UN NÚMERO",
          minlength: "EL AÑO DEBE TENER 4 CARACTERES",
          maxlength: "EL AÑO DEBE TENER 4 CARACTERES"
        },
        "region_equi": {
          required: "DEBE INGRESAR LA REGIÓN DEL EQUIPO",
          lettersonly: "DEBE CONTENER SOLO LETRAS",
          minlength: "LA REGIÓN DEBE TENER AL MENOS 2 CARACTERES",
          maxlength: "LA REGIÓN NO PUEDE TENER MÁS DE 25 CARACTERES"
        },
        "numero_titulos_equi": {
          required: "DEBE INGRESAR EL NÚMERO DE TÍTULOS",
          digits: "EL NÚMERO DE TÍTULOS DEBE SER UN NÚMERO"
        }
      },
    errorClass: "text-danger"
});
</script>