<h1>
  <b>
    <i class="fa fa-plus-circle"></i>
    NUEVO EQUIPO
  </b>
</h1>
<br>
<form action="<?php echo site_url('equipos/guardarEquipo');?>" method="post" enctype="multipart/form-data" id="frm_nuevo_equipo">
  <label for="nombre_equi">NOMBRE:</label>
  <input type="text" name="nombre_equi" id="nombre_equi" class="form-control" placeholder="Ingrese el nombre">
  <br>

  <label for="siglas_equi">SIGLAS:</label>
  <input type="text" name="siglas_equi" id="siglas_equi" class="form-control" placeholder="Ingrese las siglas">
  <br>

  <label for="fundacion_equi">AÑO DE FUNDACIÓN:</label>
  <input type="number" name="fundacion_equi" id="fundacion_equi" class="form-control" placeholder="Ingrese el año de fundación">
  <br>

  <label for="region_equi">REGIÓN:</label>
  <input type="text" name="region_equi" id="region_equi" class="form-control" placeholder="Ingrese la región">
  <br>

  <label for="numero_titulos_equi">NÚMERO DE TÍTULOS:</label>
  <input type="number" name="numero_titulos_equi" id="numero_titulos_equi" class="form-control" placeholder="Ingrese el número de títulos">
  <br>

  <div class="row">
    <div class="col-md-12 text-center">
      <button type="submit" name="button" class="btn btn-primary">
        <i class="fa-solid fa-floppy-disk"></i> GUARDAR
      </button>
      <a href="<?php echo site_url('equipos/index');?>" class="btn btn-danger">
        <i class="fa-solid fa-ban"></i> CANCELAR
      </a>
    </div>
  </div>
</form>
<br><br>
<script>
     $.validator.addMethod("lettersonly", function(value, element) {
    return this.optional(element) || /^[a-zA-Z\s]+$/i.test(value);
  }, "DEBE CONTENER SOLO LETRAS");

$("#frm_nuevo_equipo").validate({
     rules: {
        "nombre_equi": {
          required: true,
          lettersonly: true,
          minlength: 2,
          maxlength: 50
        },
        "siglas_equi": {
          required: true,
          minlength: 2,
          maxlength: 25
        },
        "fundacion_equi": {
          required: true,
          digits: true,
          minlength: 4,
          maxlength: 4
        },
       "region_equi": {
          required: true,
          lettersonly: true,
          minlength: 2,
          maxlength: 25
        },
        "numero_titulos_equi": {
          required: true,
          digits: true
        }
      },
      messages: {
        "nombre_equi": {
          required: "DEBE INGRESAR EL NOMBRE DEL EQUIPO",
          lettersonly: "DEBE CONTENER SOLO LETRAS",
          minlength: "EL NOMBRE DEBE TENER AL MENOS 2 CARACTERES",
          maxlength: "EL NOMBRE NO PUEDE TENER MÁS DE 50 CARACTERES"
        },
        "siglas_equi": {
          required: "DEBE INGRESAR LAS SIGLAS DEL EQUIPO",
          minlength: "LAS SIGLAS DEBEN TENER AL MENOS 2 CARACTERES",
          maxlength: "LAS SIGLAS NO PUEDEN TENER MÁS DE 25 CARACTERES"
        },
        "fundacion_equi": {
          required: "DEBE INGRESAR EL AÑO DE FUNDACIÓN",
          digits: "EL AÑO DEBE SER UN NÚMERO",
          minlength: "EL AÑO DEBE TENER 4 CARACTERES",
          maxlength: "EL AÑO DEBE TENER 4 CARACTERES"
        },
        "region_equi": {
          required: "DEBE INGRESAR LA REGIÓN DEL EQUIPO",
          lettersonly: "DEBE CONTENER SOLO LETRAS",
          minlength: "LA REGIÓN DEBE TENER AL MENOS 2 CARACTERES",
          maxlength: "LA REGIÓN NO PUEDE TENER MÁS DE 25 CARACTERES"
        },
        "numero_titulos_equi": {
          required: "DEBE INGRESAR EL NÚMERO DE TÍTULOS",
          digits: "EL NÚMERO DE TÍTULOS DEBE SER UN NÚMERO"
        }
      },
    errorClass: "text-danger"
});
</script>