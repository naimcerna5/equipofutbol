<h1>
<i class="fa fa-trophy"></i>
EQUIPOS
</h1>
<div class="row">
  

    <!-- Button trigger modal -->
   
  
    <div class="row">
  <div class="col-md-12 text-end">

  
<?php if ($this->session->flashdata("confirmacion")): ?>
    <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata("confirmacion"); ?>
    </div>
<?php endif; ?>


  <a href="<?php echo site_url('equipos/nuevo'); ?>" class="btn btn-primary mb-3">
    <i class="fa fa-plus-circle"></i> Nuevo Equipo
</a>
    <br><br>
  </div>
</div>
  <br><br>
  </div>
</div>
<?php if ($listadoEquipos): ?>
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>ID</th>
          <th>NOMBRE</th>
          <th>SIGLAS</th>
          <th>FUNDACIÓN</th>
          <th>REGIÓN</th>
          <th>NÚMERO DE TÍTULOS</th>
          <th>ACCIONES</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($listadoEquipos as $equipo): ?>
          <tr>
            <td> <?php echo $equipo->id_equi; ?> </td>
            <td> <?php echo $equipo->nombre_equi; ?> </td>
            <td> <?php echo $equipo->siglas_equi; ?> </td>
            <td> <?php echo $equipo->fundacion_equi; ?> </td>
            <td> <?php echo $equipo->region_equi; ?> </td>
            <td> <?php echo $equipo->numero_titulos_equi; ?> </td>
            <td>
              <a href="<?php echo site_url('equipos/editar/').$equipo->id_equi; ?>"
                   class="btn btn-warning"
                   title="Editar">
                <i class="fa fa-pen"></i>
              </a>
              <a href="<?php echo site_url('equipos/borrar/').$equipo->id_equi; ?>"
                   class="btn btn-danger"
                   title="Eliminar">
                <i class="fa fa-trash"></i>
              </a>
          </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>

   
<?php else: ?>
  <div class="alert alert-danger">
      No se encontraron equipos registrados
  </div>
<?php endif; ?>
