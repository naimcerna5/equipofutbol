<!doctype html>
<html lang="en">
  <head>
    <title>Banker &mdash; Website Template by Colorlib</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url('fonts/icomoon/style.css'); ?>">

    <link rel="stylesheet" href="<?php echo base_url('static/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('static/css/jquery-ui.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('static/css/owl.carousel.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('static/css/owl.theme.default.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('static/css/owl.theme.default.min.css'); ?>">

    <link rel="stylesheet" href="<?php echo base_url('static/css/jquery.fancybox.min.css'); ?>">

    <link rel="stylesheet" href="<?php echo base_url('static/css/bootstrap-datepicker.css'); ?>">

    <link rel="stylesheet" href="<?php echo base_url('fonts/flaticon/font/flaticon.css'); ?>">

    <link rel="stylesheet" href="<?php echo base_url('static/css/aos.css'); ?>">

    <link rel="stylesheet" href="<?php echo base_url('static/css/style.css'); ?>">

  </head>
  <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">


  <div id="overlayer"></div>
  <div class="loader">
    <div class="spinner-border text-primary" role="status">
      <span class="sr-only">Loading...</span>
    </div>
  </div>


  <div class="site-wrap">

    <div class="site-mobile-menu site-navbar-target">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>


    <header class="site-navbar js-sticky-header site-navbar-target" role="banner">

      <div class="container">
        <div class="row align-items-center">

          <div class="col-6 col-xl-2">
              <div class="col-md-6">
                <img src="<?php echo base_url('static/img/logo.svg'); ?>" alt="" style="width: 400px;">
              </div>


          </div>

          <div class="col-12 col-md-10 d-none d-xl-block">
            <nav class="site-navigation position-relative text-right" role="navigation">

              <ul class="site-menu main-menu js-clone-nav mr-auto d-none d-lg-block">
                <li><a href="<?php echo base_url('#home-section'); ?>" class="nav-link">Inicio</a></li>
                <li class="has-children">
                  <a href="<?php echo base_url('#about-section'); ?>" class="nav-link">Conoce mas</a>
                  <ul class="dropdown">
                    <li><a href="<?php echo base_url('#gallery-section'); ?>" class="nav-link">Galeria</a></li>
                    <li><a href="<?php echo base_url('#services-section'); ?>" class="nav-link">Servicios</a></li>
                    <li><a href="<?php echo base_url('#testimonials-section'); ?>" class="nav-link">Testimonio</a></li>
                  </ul>
                </li>


                <li><a href="<?php echo base_url('#blog-section'); ?>" class="nav-link">Blog</a></li>
                <li><a href="<?php echo base_url('#contact-section'); ?>" class="nav-link">Contactanos</a></li>
                <li class="social"><a href="<?php echo base_url('#contact-section'); ?>" class="nav-link"><span class="icon-facebook"></span></a></li>
                <li class="social"><a href="<?php echo base_url('#contact-section'); ?>" class="nav-link"><span class="icon-twitter"></span></a></li>
                <li class="social"><a href="<?php echo base_url('#contact-section'); ?>" class="nav-link"><span class="icon-linkedin"></span></a></li>
                <a class="btn btn-primary mr-2 mb-2"  href="<?php echo site_url('agencias/index');?>">Sistema Financiero</a>

              </ul>
            </nav>
          </div>


          <div class="col-6 d-inline-block d-xl-none ml-md-0 py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle float-right"><span class="icon-menu h3"></span></a></div>

        </div>
      </div>

    </header>
