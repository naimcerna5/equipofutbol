<h1><i class="fa fa-users"></i> Listado de Jugadores</h1>
<div class="row">
  <div class="col-md-12 text-end">

  
<?php if ($this->session->flashdata("confirmacion")): ?>
    <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata("confirmacion"); ?>
    </div>
<?php endif; ?>


  <a href="<?php echo site_url('jugadores/nuevo'); ?>" class="btn btn-primary mb-3">
    <i class="fa fa-plus-circle"></i> Nuevo Jugador
</a>
    <br><br>
  </div>
</div>

<?php if ($listadoJugadores): ?>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>ID</th>
                <th>Apellido</th>
                <th>Nombre</th>
                <th>Estatura</th>
                <th>Salario</th>
                <th>Estado</th>
                <th>Posición</th>
                <th>Equipo</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($listadoJugadores as $jugador): ?>
                <tr>
                    <td><?php echo $jugador->id_jug; ?></td>
                    <td><?php echo $jugador->apellido_jug; ?></td>
                    <td><?php echo $jugador->nombre_jug; ?></td>
                    <td><?php echo $jugador->estatura_jug; ?></td>
                    <td><?php echo $jugador->salario_jug; ?></td>
                    <td><?php echo $jugador->estado_jug; ?></td>
                    <td><?php echo $jugador->nombre_pos; ?></td>
                    <td><?php echo $jugador->nombre_equi; ?></td>
                    <td>
                        <a href="<?php echo site_url('jugadores/editar/') . $jugador->id_jug; ?>" class="btn btn-warning" title="Editar">
                            <i class="fa fa-pen"></i>
                        </a>
                        <a href="<?php echo site_url('jugadores/borrar/') . $jugador->id_jug; ?>" class="btn btn-danger" title="Eliminar" onclick="return confirm('¿Estás seguro de eliminar este jugador?');">
                        <i class="fa fa-trash"></i>

                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <br><br><br><br><br><br><br><br><br><br><br><br>
<?php else: ?>
    <div class="alert alert-danger">
        No se encontraron jugadores registrados
    </div>
    
<?php endif; ?>
