<h1>Editar Jugador</h1>

<form action="<?php echo site_url('jugadores/actualizarJugador');?>" method="post" enctype="multipart/form-data" id="frm_nuevo_jugador">
    <input type="hidden" name="id_jug" id="id_jug" value="<?php echo $jugadorEditar->id_jug; ?>">

    <label for="apellido_jug" class="error-message">Apellido:</label>
    <input type="text" name="apellido_jug" id="apellido_jug" class="form-control" value="<?php echo $jugadorEditar->apellido_jug; ?>" required>
    <br>

    <label for="nombre_jug" class="error-message">Nombre:</label>
    <input type="text" name="nombre_jug" id="nombre_jug" class="form-control" value="<?php echo $jugadorEditar->nombre_jug; ?>" required>
    <br>

    <label for="estatura_jug">Estatura:</label>
    <input type="number" step="0.01" name="estatura_jug" id="estatura_jug" class="form-control" value="<?php echo $jugadorEditar->estatura_jug; ?>" required>
    <br>

    <label for="salario_jug">Salario:</label>
    <input type="text" name="salario_jug" id="salario_jug" class="form-control" value="<?php echo $jugadorEditar->salario_jug; ?>" required>
    <br>

    <label for="estado_jug">Estado:</label>
    <input type="text" name="estado_jug" id="estado_jug" class="form-control" value="<?php echo $jugadorEditar->estado_jug; ?>" required>
    <br>

    <label for="fk_id_pos">Posición:</label>
    <select name="fk_id_pos" id="fk_id_pos" class="form-control" required>
        <option value="">Seleccionar posición</option>
        <?php foreach ($posiciones as $posicion): ?>
            <option value="<?php echo $posicion->id_pos; ?>" <?php echo ($posicion->id_pos == $jugadorEditar->fk_id_pos) ? 'selected' : ''; ?>><?php echo $posicion->nombre_pos; ?></option>
        <?php endforeach; ?>
    </select>
    <br>

    <label for="fk_id_equi">Equipo:</label>
    <select name="fk_id_equi" id="fk_id_equi" class="form-control" required>
        <option value="">Seleccionar equipo</option>
        <?php foreach ($equipos as $equipo): ?>
            <option value="<?php echo $equipo->id_equi; ?>" <?php echo ($equipo->id_equi == $jugadorEditar->fk_id_equi) ? 'selected' : ''; ?>><?php echo $equipo->nombre_equi; ?></option>
        <?php endforeach; ?>
    </select>
    <br>

    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button" class="btn btn-primary"> <i class="fa fa-pen"></i> &nbsp; Actualizar</button>&nbsp;&nbsp;
            <a href="<?php echo site_url('jugadores/index'); ?>" class="btn btn-danger"> <i class="fa fa-times-circle"></i> &nbsp; Cancelar</a>
        </div>
    </div>
</form>

<br><br>

<script>
  $.validator.addMethod("lettersonly", function(value, element) {
    return this.optional(element) || /^[a-zA-Z\s]+$/i.test(value);
  }, "DEBE CONTENER SOLO LETRAS");

  $("#frm_nuevo_jugador").validate({
    rules: {
      "apellido_jug": {
        required: true,
        lettersonly: true,
        minlength: 2,
        maxlength: 50
      },
      "nombre_jug": {
        required: true,
        lettersonly: true,
        minlength: 2,
        maxlength: 50
      },
      "estatura_jug": {
        required: true,
        minlength: 2,
        maxlength: 25
      },
      "salario_jug": {
        required: true,
        digits: true,
        minlength: 4,
        maxlength: 4
      },
      "estado_jug": {
        required: true,
        lettersonly: true,
        minlength: 2,
        maxlength: 25
      }
     
    },
    messages: {
      "apellido_jug": {
        required: "DEBE INGRESAR EL APELLIDO DEL JUGADOR",
        lettersonly: "DEBE CONTENER SOLO LETRAS",
        minlength: "EL APELLIDO DEBE TENER AL MENOS 2 CARACTERES",
        maxlength: "EL APELLIDO NO PUEDE TENER MÁS DE 50 CARACTERES"
      },
      "nombre_jug": {
        required: "DEBE INGRESAR EL NOMBRE DEL JUGADOR",
        lettersonly: "DEBE CONTENER SOLO LETRAS",
        minlength: "EL NOMBRE DEBE TENER AL MENOS 2 CARACTERES",
        maxlength: "EL NOMBRE NO PUEDE TENER MÁS DE 50 CARACTERES"
      },
      "estatura_jug": {
        required: "DEBE INGRESAR LA ESTATURA DEL JUGADOR",
        minlength: "LA ESTATURA DEBE TENER AL MENOS 2 CARACTERES",
        maxlength: "LA ESTATURA NO PUEDE TENER MÁS DE 25 CARACTERES"
      },
      "salario_jug": {
        required: "DEBE INGRESAR EL SALARIO DEL JUGADOR",
        digits: "EL SALARIO DEBE SER UN NÚMERO",
        minlength: "EL SALARIO DEBE TENER 4 CARACTERES",
        maxlength: "EL SALARIO DEBE TENER 4 CARACTERES"
      },
      "estado_jug": {
        required: "DEBE INGRESAR EL ESTADO DEL JUGADOR",
        lettersonly: "DEBE CONTENER SOLO LETRAS",
        minlength: "EL ESTADO DEBE TENER AL MENOS 2 CARACTERES",
        maxlength: "EL ESTADO NO PUEDE TENER MÁS DE 25 CARACTERES"
      }
    },
    errorClass: "text-danger"
  });
</script>