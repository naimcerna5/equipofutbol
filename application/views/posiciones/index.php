<h1><i class="fa fa-sitemap"></i> POSICIONES</h1>
<div class="row">
  <div class="col-md-12 text-end">

  
<?php if ($this->session->flashdata("confirmacion")): ?>
    <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata("confirmacion"); ?>
    </div>
<?php endif; ?>


  <a href="<?php echo site_url('posiciones/nuevo'); ?>" class="btn btn-primary mb-3">
    <i class="fa fa-plus-circle"></i> Nueva Posicion
</a>
    <br><br>
  </div>
</div>

<?php if ($listadoPosiciones): ?>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>ID</th>
        <th>NOMBRE</th>
        <th>DESCRIPCIÓN</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoPosiciones as $posicion): ?>
        <tr>
          <td><?php echo $posicion->id_pos; ?></td>
          <td><?php echo $posicion->nombre_pos; ?></td>
          <td><?php echo $posicion->descripcion_pos; ?></td>
          <td>
            <a href="<?php echo site_url('posiciones/editar/') . $posicion->id_pos; ?>" class="btn btn-warning" title="Editar">
              <i class="fa fa-pen"></i>
            </a>
            <a href="<?php echo site_url('posiciones/borrar/') . $posicion->id_pos; ?>" class="btn btn-danger" title="Eliminar" onclick="return confirm('¿Estás seguro de eliminar esta posición?');">
              Eliminar
            </a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>

  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">
            <i class="fa fa-eye"></i> MAPA DE POSICIONES
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </h5>
        </div>
        <div class="modal-body">
          <div id="reporteMapa" style="height:300px; width:100%; border:2px solid black;"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>

<?php else: ?>
  <div class="alert alert-danger">
    No se encontraron posiciones registradas
  </div>
<?php endif; ?>
