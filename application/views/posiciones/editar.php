<h1>EDITAR POSICIÓN</h1>
<form class="" method="post" action="<?php echo site_url('posiciones/actualizarPosicion'); ?>" id="frm_nueva_posicion">
  <input type="hidden" name="id_pos" id="id_pos" value="<?php echo $posicionEditar->id_pos; ?>">
  <label for=""><b>Nombre:</b></label>
  <input type="text" name="nombre_pos" id="nombre_pos" value="<?php echo $posicionEditar->nombre_pos; ?>" placeholder="Ingrese el nombre..." class="form-control" required>
  <br>
  <label for=""><b>Descripción:</b></label>
  <textarea name="descripcion_pos" id="descripcion_pos" placeholder="Ingrese la descripción..." class="form-control" required><?php echo $posicionEditar->descripcion_pos; ?></textarea>
  <br>

  <br>

  <br>
  <div class="row">
    <div class="col-md-12 text-center">
      <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-pen"></i> &nbsp; ACTUALIZAR</button>&nbsp;&nbsp;
      <a href="<?php echo site_url('posiciones/index'); ?>" class="btn btn-danger"> <i class="fa fa-times-circle"></i> &nbsp; Cancelar</a>
    </div>
  </div>
</form>

<br><br>

<script>
  $.validator.addMethod("lettersonly", function(value, element) {
    return this.optional(element) || /^[a-zA-Z\s.,!?]+$/i.test(value);
  }, "DEBE CONTENER SOLO LETRAS, ESPACIOS Y SIGNOS DE PUNTUACIÓN");

  $("#frm_nueva_posicion").validate({
    rules: {
      "nombre_pos": {
        required: true,
        lettersonly: true,
        minlength: 2,
        maxlength: 150
      },
      "descripcion_pos": {
        required: true,
        lettersonly: true,
        minlength: 2,
        maxlength: 500 // Ajusta el valor según sea necesario
      }
    },
    messages: {
      "nombre_pos": {
        required: "INGRESE EL NOMBRE DE LA POSICIÓN",
        lettersonly: "DEBE CONTENER SOLO LETRAS, ESPACIOS Y SIGNOS DE PUNTUACIÓN",
        minlength: "DEBE TENER AL MENOS 2 CARACTERES",
        maxlength: "NO PUEDE TENER MÁS DE 150 CARACTERES"
      },
      "descripcion_pos": {
        required: "INGRESE LA DESCRIPCIÓN DE LA POSICIÓN",
        lettersonly: "DEBE CONTENER SOLO LETRAS, ESPACIOS Y SIGNOS DE PUNTUACIÓN",
        minlength: "DEBE TENER AL MENOS 2 CARACTERES",
        maxlength: "NO PUEDE TENER MÁS DE 500 CARACTERES" // Ajusta el valor según sea necesario
      }
    },
    errorClass: "text-danger"
  });
</script>