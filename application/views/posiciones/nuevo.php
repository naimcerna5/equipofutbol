<h1>
  <b>
  <i class="fa fa-plus-circle"></i>
  NUEVA POSICIÓN
  </b>
</h1>
<br>
<form action="<?php echo site_url('posiciones/guardarPosicion');?>" method="post" enctype="multipart/form-data" id="frm_nueva_posicion">
  <label for="nombre_pos" class="error-message">NOMBRE:</label>
  <input type="text" name="nombre_pos" id="nombre_pos" class="form-control" placeholder="Ingrese el nombre">
  <br>

  <label for="descripcion_pos" class="error-message">DESCRIPCIÓN:</label>
  <textarea name="descripcion_pos" id="descripcion_pos" class="form-control" placeholder="Ingrese la descripción"></textarea>
  <br>





  
  <br>

  <div class="row">
    <div class="col-md-12 text-center">
      <button type="submit" name="button" class="btn btn-primary"> <i class="fa-solid fa-floppy-disk"></i> GUARDAR</button>
      <a href="<?php echo site_url('posiciones/index');?>" class="btn btn-danger"> <i class="fa-solid fa-ban"></i>
        CANCELAR
      </a>
    </div>
  </div>
</form>
<br><br>


<script>
  $.validator.addMethod("lettersonly", function(value, element) {
    return this.optional(element) || /^[a-zA-Z\s.,!?]+$/i.test(value);
  }, "DEBE CONTENER SOLO LETRAS, ESPACIOS Y SIGNOS DE PUNTUACIÓN");

  $("#frm_nueva_posicion").validate({
    rules: {
      "nombre_pos": {
        required: true,
        lettersonly: true,
        minlength: 2,
        maxlength: 150
      },
      "descripcion_pos": {
        required: true,
        lettersonly: true,
        minlength: 2,
        maxlength: 500 // Ajusta el valor según sea necesario
      }
    },
    messages: {
      "nombre_pos": {
        required: "INGRESE EL NOMBRE DE LA POSICIÓN",
        lettersonly: "DEBE CONTENER SOLO LETRAS, ESPACIOS Y SIGNOS DE PUNTUACIÓN",
        minlength: "DEBE TENER AL MENOS 2 CARACTERES",
        maxlength: "NO PUEDE TENER MÁS DE 150 CARACTERES"
      },
      "descripcion_pos": {
        required: "INGRESE LA DESCRIPCIÓN DE LA POSICIÓN",
        lettersonly: "DEBE CONTENER SOLO LETRAS, ESPACIOS Y SIGNOS DE PUNTUACIÓN",
        minlength: "DEBE TENER AL MENOS 2 CARACTERES",
        maxlength: "NO PUEDE TENER MÁS DE 500 CARACTERES" // Ajusta el valor según sea necesario
      }
    },
    errorClass: "text-danger"
  });
</script>