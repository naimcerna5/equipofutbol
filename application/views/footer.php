</div>
</div>
</div>
</div>
</div>
</div>
</div>
<?php if ($this->session->flashdata('confirmacion')): ?>
<script type="text/javascript">
Swal.fire({
title: "CONFIRMACION",
text: "<?php echo $this->session->flashdata('confirmacion');?>",
icon: "success"
})

</script>
<?php $this->session->set_flashdata('confirmacion',''); ?>
<?php endif; ?>

</div>
<script src="<?php echo base_url('assets/js/vendor-all.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/plugins/bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/ripple.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/pcoded.min.js'); ?>"></script>

<!-- Apex Chart -->
<script src="<?php echo base_url('assets/js/plugins/apexcharts.min.js'); ?>"></script>

<!-- custom-chart js -->
<script src="<?php echo base_url('assets/js/pages/dashboard-main.js'); ?>"></script>
</body>
</html>
